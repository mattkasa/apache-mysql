FROM docker.io/bitnami/minideb:bullseye AS source

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN install_packages libaprutil1-dbd-mysql

FROM bitnami/apache

COPY --from=source /usr/lib/x86_64-linux-gnu/libmariadb.so.3 /usr/lib/x86_64-linux-gnu/libmariadb.so.3
COPY --from=source /usr/lib/x86_64-linux-gnu/apr-util-1/apr_dbd_mysql* /opt/bitnami/apache/lib/apr-util-1/
